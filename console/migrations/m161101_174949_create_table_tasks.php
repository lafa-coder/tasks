<?php

use yii\db\Migration;

class m161101_174949_create_table_tasks extends Migration
{
    public function up()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'type_id' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('tasks');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
