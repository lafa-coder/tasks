<?php

use yii\db\Migration;

class m161030_183841_create_type_tasks extends Migration
{
    public function up()
    {
        $this->createTable('task_types', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'position' => $this->integer(),
            'is_default' => $this->boolean()->defaultValue(0),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime()
        ]);
    }

    public function down()
    {
        $this->dropTable('task_types');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
