<?php

use yii\db\Migration;

class m161102_100519_updgrade_tasks extends Migration
{
    public function up()
    {
        $this->addColumn('tasks', 'description', $this->text());
    }

    public function down()
    {
        $this->dropColumn('tasks', 'description');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
