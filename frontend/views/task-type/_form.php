<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\sortable\Sortable;
/* @var $this yii\web\View */
/* @var $model common\models\TaskType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= ''/*$form->field($model, 'position')->widget(Select2::classname(), [
        'data' => $model->getTypeList(),
        'language' => 'ru',
        'options' => ['placeholder' => 'Тип'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);  */?>

    <?=''
    /*Sortable::widget([
        'type' => 'grid',
        'items' => $model->getTypeList()
    ])*/;
    ?>

    <?= $form->field($model, 'is_default')->radio() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
